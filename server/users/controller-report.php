<?php
namespace server\users;

use server\Tools;
use server\users\CRUD_Read as Read_User;

class Controller_Report {

    public static function init() {
        if (isset($_POST['report'])) {
            switch ($_POST['report']) {
                case "users":
                    // Creando una variables Sesion para proteger la vista de reportes
                    session_start();
                    $_SESSION['token'] = 'Hakyn_Reportes_Locos';

                    Tools::res_code(202, ["token" => password_hash($_SESSION['token'], PASSWORD_DEFAULT)]);
                    break;
                default:
                    Tools::res_code(500);
                    break;
            }
        } else Tools::res_code(500);
    }

    public static function init_report() {
        self::generate_pdf();
    }

    public static function generate_pdf () {
        // Obteniendo todos los usuarios de la base de datos
        $users =  Read_User::all_users("data");

        $pdf = new PDF;
        $pdf -> AliasNbPages();
        $pdf -> AddPage();
        $pdf -> SetFont('Arial', '', 8);

        foreach($users as $user) {
            $pdf -> Cell(3, 7, $user -> get_id(), 0, 0, 'C');
            $pdf -> Cell(25, 7, utf8_decode($user -> get_name()), 0, 0, 'C');
            $pdf -> Cell(30, 7, utf8_decode($user -> get_surname()), 0, 0, 'C');
            $pdf -> Cell(30, 7, utf8_decode($user -> get_surname_second()), 0, 0, 'C');
            $pdf -> Cell(15, 7, utf8_decode($user -> get_gender()), 0, 0, 'C');
            $pdf -> Cell(20, 7, $user -> get_cel()['celular'][0], 0, 0, 'C');
            $pdf -> Cell(25, 7, utf8_decode($user -> get_rank_data()), 0, 0, 'C');
            $pdf -> Cell(25, 7, $user -> get_create_at(), 0, 0, 'C');
            $pdf -> Cell(25, 7, $user -> get_update_at(), 0, 1, 'C');
        }

        $pdf -> Output();
    }

}

class PDF extends \FPDF {
    function Header() {
        $this -> SetFont('Arial', 'B', 15);
        // Movernos a la derecha
        $this -> Cell(80);
        $this -> Cell(30, 10, 'Reporte de Usuarios Registrados', 0, 0, 'C');
        $this->Ln(20);

        $this -> SetFont('Arial', 'B', 8);
        $this -> Cell(3, 7, "id", 0, 0, 'C');
        $this -> Cell(25, 7, "Nombre", 0, 0, 'C');
        $this -> Cell(30, 7, "Apellido Paterno", 0, 0, 'C');
        $this -> Cell(30, 7, "Apellido Materno", 0, 0, 'C');
        $this -> Cell(15, 7, "Genero", 0, 0, 'C');
        $this -> Cell(20, 7, "Celular", 0, 0, 'C');
        $this -> Cell(25, 7, "Rango", 0, 0, 'C');
        $this -> Cell(25, 7, "Creado", 0, 0, 'C');
        $this -> Cell(25, 7, "Actualizado", 0, 1, 'C');
    }

    function Footer() {
        $this -> SetY(-10);
        $this -> SetX(-370);
        $this -> setFont('Arial', 'I', 8);
        $this -> Cell(0, 10, utf8_decode('Página ').$this -> PageNo().' de {nb}', 0, 0, 'C');
        $this -> SetX(-100);
        $this -> Cell(0, 10, utf8_decode('Joaquin Reyes Sánchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>'), 0, 0, 'C');
    }
}
