<?php
namespace server\users;

use config\exceptions\DbException;
use server\Tools;
use server\users\{
    Data as Data_User, 
    CRUD_Create as Create_User, 
};

class Controller_Create {

    public static function init() {
       if (isset($_POST)) {
           if ( count($_POST) > 0 ) {
               self::create_user();
            } else Tools::res_code(500); 
       } else Tools::res_code(500);
    }

    private static function create_user() { 
        // TODO: Aqui tendría que ir un validador más en forma
        $error = [];
        $error['name'] = Tools::validator_simple('name', 3, 7, 'string');
        $error['surname'] = Tools::validator_simple('surname', 3, 7, 'string');
        $error['surname_second'] = Tools::validator_simple('surname_second', 3, 7, 'string');
        $error['gender'] = Tools::validator_simple('gender', 0, 10, 'string'); // Este validador lo realizará en la clase Data
        $error['cel'] = Tools::validator_simple('cel', 10, 10, 'number');
        $error['rank'] = Tools::validator_simple('rank', 0, 10, 'number');

        $validator = false;
        if (
            empty($error['name']) && 
            empty($error['surname']) &&
            empty($error['surname_second']) &&
            empty($error['gender']) &&
            empty($error['cel']) &&
            empty($error['rank'])
        ) $validator = true;

        if ($validator) {
            // Guardando los datos del formulario en la clase User
            $user = new Data_User();
            $user -> set_name($_POST['name']);
            $user -> set_surname($_POST['surname']);
            $user -> set_surname_second($_POST['surname_second']);
            $user -> set_gender($_POST['gender']);
            $user -> set_cel(array(
                "celular" => [$_POST['cel']]
            ));
            $user -> set_rank((int)$_POST['rank']);

            if (Create_User::create($user)) Tools::res_code(202);
            else throw new DbException('DB-002');
        } else Tools::res_code(406, ["Error" => "No logró pasar la validación del Servidor", "Validate" => $error]);
    }
}
