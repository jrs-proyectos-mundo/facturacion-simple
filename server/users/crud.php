<?php
namespace server\users;

use config\DB;
use server\users\SQL;
use server\users\Data;

function generate_user ($data) {
    $user = array();

    $user['id'] = (int)$data['id'];
    $user['name'] = $data['name'];
    $user['surname'] = $data['surname'];
    $user['surname_second'] = $data['surname_second'];
    $user['gender'] = $data['gender'];
    $number = json_decode($data['cel'], true);
    $user['cel'] = $number['celular'];
    $user['id_rank'] = $data['id_rank'];
    $user['rank'] = $data['rank'];

    return $user;
}

function generate_user_data ($data) {
    $user = new Data();

    $user -> set_id((int)$data['id']);
    $user -> set_name($data['name']);
    $user -> set_surname($data['surname']);
    $user -> set_surname_second($data['surname_second']);
    $user -> set_gender($data['gender']);
    $user -> set_cel(json_decode($data['cel'], true));
    $user -> set_rank((int)$data['id_rank']);
    $user -> set_rank_data($data['rank']);
    $user -> set_create_at($data['create_at']);
    $user -> set_update_at($data['create_at']);

    return $user;
}

class CRUD_Read {
    /**
     * Método para recuperar todos los usuarios
     *
     * @return Array {Usuarios} 
     */
    public static function all_users ($output = "array") {
        $users = array();

        $conn = DB::init() -> conn();

        if (isset($conn)) {
            $stmt = $conn -> query(SQL::read_all);
            
            foreach($stmt -> fetchAll() as $row) {

                if ($output === 'array') 
                    $user = generate_user($row);
                elseif ($output === 'data')
                    $user = generate_user_data($row);
                else
                    $user = [];

                array_push($users, $user);
            }
        }

        return $users;
    }
}

class CRUD_Create {
    /**
     * Método para registrar un nuevo usuario
     *
     * @param user [object DATA] {Obtejeto del usuario}
     * @return bool {True -> Usuario creado, False -> Error al crear el usuario} 
     */
    public static function create ($user) {
        $success = false;
        
        $conn = DB::init() -> conn();

        if (isset($conn)) {
            $stmt = $conn -> prepare(SQL::create);
            $stmt -> bindValue('name', $user -> get_name());
            $stmt -> bindValue('surname', $user -> get_surname());
            $stmt -> bindValue('surname_second', $user -> get_surname_second());
            $stmt -> bindValue('gender', $user -> get_gender());
            $stmt -> bindValue('cel', json_encode($user -> get_cel()));
            $stmt -> bindValue('rank', $user -> get_rank());

            if ($stmt -> execute()) $success = true;
        }
        
        return $success;
    }
}

class CRUD_Update {
    /**
     * Método para actualizar un usuario
     *
     * @param user [object DATA] {Obtejeto del usuario}
     * @return bool {True -> Usuario creado, False -> Error al crear el usuario} 
     */
    public static function update ($user) {
        $success = false;
        
        $conn = DB::init() -> conn();
        
        if (isset($conn)) {
            $stmt = $conn -> prepare(SQL::update);
            $stmt -> bindValue('id', $user -> get_id());
            $stmt -> bindValue('name', $user -> get_name());
            $stmt -> bindValue('surname', $user -> get_surname());
            $stmt -> bindValue('surname_second', $user -> get_surname_second());
            $stmt -> bindValue('gender', $user -> get_gender());
            $stmt -> bindValue('cel', json_encode($user -> get_cel()));
            $stmt -> bindValue('rank', $user -> get_rank());

            if ($stmt -> execute()) $success = true;
        }
        
        return $success;
    }
}


class CRUD_Delete {
    /**
     * Método para eliminar un usuario
     *
     * @param user [object DATA] {Obtejeto del usuario}
     * @return bool {True -> Usuario creado, False -> Error al crear el usuario} 
     */
    public static function delete ($user) {
        $success = false;
        
        $conn = DB::init() -> conn();
        
        if (isset($conn)) {
            $stmt = $conn -> prepare(SQL::delete);
            $stmt -> bindValue('id', $user -> get_id());

            if ($stmt -> execute()) $success = true;
        }
        
        return $success;
    }
}
