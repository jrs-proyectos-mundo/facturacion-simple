@startuml
'==========================
' Configuraciones Básicas |
'==========================
    header Sistema de Facturación Simple
    footer Autor: Joaquin Reyes Sánchez [Hakyn Seyer] <joaquin.seyer21@gmail.com>

    title UML Base de Datos

    skinparam Padding 2
    skinparam class {
        BackgroundColor Wheat
        ArrowColor DeepPink
        BorderColor MediumVioletRed
    }
    skinparam ClassFontSize 18
    skinparam NoteBackgroundColor SpringGreen
    skinparam NoteFontSize 9

'=============
' Users_Rank |
'=============
    skinparam ClassHeaderBackgroundColor<<users_rank>> crimson
    skinparam ClassFontColor<<users_rank>> white
    skinparam ClassStereotypeFontColor<<users_rank>> crimson
    class users_rank <<(R, white) users_rank>> {
        + id: <b>Int | AI PK
        + rank: <b>Varchar[20] | NN UQ
        + create_at: <b>Date | NN
        + update_at: <b>Date | NN
    }
'========
' Users |
'========
    skinparam ClassHeaderBackgroundColor<<users>> red
    skinparam ClassFontColor<<users>> white
    skinparam ClassStereotypeFontColor<<users>> red
    class users <<(U, white) users>> {
        + id: <b>Int | AI PK
        + name: <b>Varchar[25] | NN
        + surname: <b>Varchar[25] | NN
        + surname_second: <b>Varchar[25] | NN
        + gender: <b>Enum[male, female] | NN
        + address: <b>Varchar[255] | NN
        + email: <b>Varchar[50] | NN UQ
        + cel: <b>JSON
        + rank <back:#crimson><color:#white> R </color></back>: <b>users_rank | NN
        + create_at: <b>Date | NN
        + update_at: <b>Date | NN
    }
    users::rank -- users_rank::id

'===================
' Products_Details |
'===================
    skinparam ClassHeaderBackgroundColor<<products_details>> mediumvioletred
    skinparam ClassFontColor<<products_details>> white
    skinparam ClassStereotypeFontColor<<products_details>> mediumvioletred
    class products_details <<(D, white) products_details>> {
        + id: <b>Int | AI PK
        + unit: <b>Enum[pza,g,kg,tonelada,bulto,bolsa] | NN
        + amount: <b>Double | NN
        + price: <b>Double | NN
        + supplier <back:#red><color:#white> U </color></back>: <b>users | NN
        + cost: <b>Double | NN
        + create_at: <b>Date | NN
        + update_at: <b>Date | NN
    }
    products_details::supplier -- users::id

'===========
' Products |
'===========
    skinparam ClassHeaderBackgroundColor<<products>> purple
    skinparam ClassFontColor<<products>> white
    skinparam ClassStereotypeFontColor<<products>> purple
    class products << (P, white) products>> {
        + id: <b>Int | AI PK
        + name: <b>Varchar[255] | NN
        + code: <b>Varchar[255] | NN UQ
        + image: <b>Varchar[255] | NN UQ
        + detail: <back:#mediumvioletred><color:#white> D </color></back>: <b>products_details | NN UQ
        + create_at: <b>Date | NN
        + update_at: <b>Date | NN
    }
    products::detail -- products_details::id

'======
' Bills |
'======
    skinparam ClassHeaderBackgroundColor<<bills>> lime
    skinparam ClassFontColor<<bills>> white
    skinparam ClassStereotypeFontColor<<bills>> lime
    class bills << (B, white) products>> {
        + id: <b>Int | AI PK
        + seller: <back:#red><color:#white> U </color></back>: <b>users | NN
        + customer: <back:#red><color:#white> U </color></back>: <b>users | NN
        + Total: <b>Double | NN
        + create_at: <b>Date | NN
        + update_at: <b>Date | NN
    }
    bills::seller -- users::id
    bills::customer -- users::id
'========
' Sales |
'========
    skinparam ClassHeaderBackgroundColor<<sales>> green
    skinparam ClassFontColor<<sales>> white
    skinparam ClassStereotypeFontColor<<sales>> green
    class sales << (S, white) products>> {
        + id: <b>Int | AI PK
        + product: <back:#purple><color:#white> P </color></back>: <b>products | NN
        + amount: <b>Double | NN
        + bill: <back:#lime><color:#white> B </color></back>: <b>bills | NN
        + create_at: <b>Date | NN
        + update_at: <b>Date | NN
    }
    sales::product -- products::id
    sales::bill -- bills::id
@enduml
